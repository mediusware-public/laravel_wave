-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 30, 2021 at 01:41 PM
-- Server version: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wave_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `title`, `description`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Wave 1.0 Released', 'We have just released the first official version of Wave. Click here to learn more!', '<p>It\'s been a fun Journey creating this awesome SAAS starter kit and we are super excited to use it in many of our future projects. There are just so many features that Wave has that will make building the SAAS of your dreams easier than ever before.</p>\n<p>Make sure to stay up-to-date on our latest releases as we will be releasing many more features down the road :)</p>\n<p>Thanks! Talk to you soon.</p>', '2018-05-20 17:19:00', '2018-05-20 18:38:02'),
(2, 'Wave 2.0 Released', 'Wave V2 has been released with some awesome new features. Be sure to read up on what\'s new!', '<p>This new version of Wave includes the following updates:</p><ul><li>Update to the latest version of Laravel</li><li>New Payment integration with Paddle</li><li>Rewritten theme support</li><li>Deployment integration</li><li>Much more awesomeness</li></ul><p>Be sure to check out the official Wave v2 page at <a href=\"https://devdojo.com/wave\">https://devdojo.com/wave</a></p>', '2020-03-20 17:19:00', '2020-03-20 18:38:02');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_user`
--

CREATE TABLE `announcement_user` (
  `announcement_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `announcement_user`
--

INSERT INTO `announcement_user` (`announcement_id`, `user_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `api_keys`
--

CREATE TABLE `api_keys` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `last_used_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2017-11-21 10:23:22', '2017-11-21 10:23:22'),
(2, NULL, 1, 'Category 2', 'category-2', '2017-11-21 10:23:22', '2017-11-21 10:23:22');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
(3, 1, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
(4, 1, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
(5, 1, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 5),
(6, 1, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
(7, 1, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(8, 1, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 8),
(9, 1, 'meta_description', 'text_area', 'meta_description', 1, 0, 1, 1, 1, 1, '', 9),
(10, 1, 'meta_keywords', 'text_area', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 10),
(11, 1, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(12, 1, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, '', 12),
(13, 1, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 13),
(14, 2, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(15, 2, 'author_id', 'text', 'author_id', 1, 0, 0, 0, 0, 0, '', 2),
(16, 2, 'title', 'text', 'title', 1, 1, 1, 1, 1, 1, '', 3),
(17, 2, 'excerpt', 'text_area', 'excerpt', 1, 0, 1, 1, 1, 1, '', 4),
(18, 2, 'body', 'rich_text_box', 'body', 1, 0, 1, 1, 1, 1, '', 5),
(19, 2, 'slug', 'text', 'slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"}}', 6),
(20, 2, 'meta_description', 'text', 'meta_description', 1, 0, 1, 1, 1, 1, '', 7),
(21, 2, 'meta_keywords', 'text', 'meta_keywords', 1, 0, 1, 1, 1, 1, '', 8),
(22, 2, 'status', 'select_dropdown', 'status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(23, 2, 'created_at', 'timestamp', 'created_at', 1, 1, 1, 0, 0, 0, '', 10),
(24, 2, 'updated_at', 'timestamp', 'updated_at', 1, 0, 0, 0, 0, 0, '', 11),
(25, 2, 'image', 'image', 'image', 0, 1, 1, 1, 1, 1, '', 12),
(26, 3, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, NULL, 1),
(27, 3, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, NULL, 2),
(28, 3, 'email', 'text', 'email', 1, 1, 1, 1, 1, 1, NULL, 3),
(29, 3, 'password', 'password', 'password', 1, 0, 0, 1, 1, 0, NULL, 5),
(30, 3, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"on\"}', 11),
(31, 3, 'remember_token', 'text', 'remember_token', 0, 0, 0, 0, 0, 0, NULL, 6),
(32, 3, 'created_at', 'timestamp', 'created_at', 0, 1, 1, 0, 0, 0, NULL, 7),
(33, 3, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, NULL, 8),
(34, 3, 'avatar', 'image', 'avatar', 0, 1, 1, 1, 1, 1, NULL, 9),
(35, 5, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(36, 5, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 2),
(37, 5, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(38, 5, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(39, 4, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(40, 4, 'parent_id', 'select_dropdown', 'parent_id', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(41, 4, 'order', 'text', 'order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(42, 4, 'name', 'text', 'name', 1, 1, 1, 1, 1, 1, '', 4),
(43, 4, 'slug', 'text', 'slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(44, 4, 'created_at', 'timestamp', 'created_at', 0, 0, 1, 0, 0, 0, '', 6),
(45, 4, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 7),
(46, 6, 'id', 'number', 'id', 1, 0, 0, 0, 0, 0, '', 1),
(47, 6, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(48, 6, 'created_at', 'timestamp', 'created_at', 0, 0, 0, 0, 0, 0, '', 3),
(49, 6, 'updated_at', 'timestamp', 'updated_at', 0, 0, 0, 0, 0, 0, '', 4),
(50, 6, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(51, 1, 'seo_title', 'text', 'seo_title', 0, 1, 1, 1, 1, 1, '', 14),
(52, 1, 'featured', 'checkbox', 'featured', 1, 1, 1, 1, 1, 1, '', 15),
(53, 3, 'role_id', 'text', 'role_id', 0, 1, 1, 1, 1, 1, NULL, 10),
(54, 3, 'username', 'text', 'Username', 1, 1, 1, 1, 1, 1, NULL, 4),
(55, 7, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(56, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 2),
(57, 7, 'description', 'text_area', 'Description (max 250 characters)', 1, 1, 1, 1, 1, 1, NULL, 3),
(58, 7, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 4),
(59, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(60, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(61, 3, 'settings', 'hidden', 'Settings', 0, 1, 1, 1, 1, 1, NULL, 9),
(62, 3, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\"}', 11),
(63, 3, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, '', 12),
(64, 8, 'id', 'hidden', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(65, 8, 'name', 'text', 'Name (Basic, Standard, Premium, etc)', 1, 1, 1, 1, 1, 1, NULL, 3),
(66, 8, 'description', 'text_area', 'Description (optional)', 0, 0, 1, 1, 1, 1, NULL, 6),
(67, 8, 'features', 'text_area', 'Features (comma separated)', 1, 0, 1, 1, 1, 1, NULL, 4),
(69, 8, 'role_id', 'text', 'Role Id', 1, 1, 1, 1, 1, 1, NULL, 2),
(70, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 0, 0, 0, 1, NULL, 8),
(71, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 9),
(72, 8, 'plan_belongsto_role_relationship', 'relationship', 'Role (role permissions for this plan)', 0, 1, 1, 1, 1, 1, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"announcement_user\",\"pivot\":\"0\",\"taggable\":\"0\"}', 5),
(73, 8, 'default', 'checkbox', 'Default (Make this the default plan)', 1, 0, 1, 1, 1, 1, NULL, 7),
(74, 8, 'price', 'text', 'Price (for display purposes only)', 1, 1, 1, 1, 1, 1, NULL, 8),
(75, 8, 'plan_id', 'text', 'Plan Id', 1, 1, 1, 1, 1, 1, NULL, 6),
(76, 8, 'trial_days', 'number', 'Trial Days (If none, set to 0)', 1, 0, 1, 1, 1, 1, NULL, 9);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2017-11-21 10:23:22', '2017-11-21 10:23:22'),
(2, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2017-11-21 10:23:22', '2017-11-21 10:23:22'),
(3, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2017-11-21 10:23:22', '2018-06-22 14:29:47'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2017-11-21 10:23:22', '2017-11-21 10:23:22'),
(5, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2017-11-21 10:23:22', '2017-11-21 10:23:22'),
(6, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2017-11-21 10:23:22', '2017-11-21 10:23:22'),
(7, 'announcements', 'announcements', 'Announcement', 'Announcements', 'voyager-megaphone', 'Wave\\Announcement', NULL, NULL, NULL, 1, 0, NULL, '2018-05-20 15:08:14', '2018-05-20 15:08:14'),
(8, 'plans', 'plans', 'Plan', 'Plans', 'voyager-logbook', 'Wave\\Plan', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-02 22:50:28', '2018-07-02 22:50:28');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2017-11-21 10:23:22', '2017-11-21 10:23:22'),
(2, 'authenticated-menu', '2017-11-28 08:47:49', '2018-04-13 16:25:28'),
(3, 'guest-menu', '2018-04-13 16:25:37', '2018-04-13 16:25:37');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2017-11-21 10:23:22', '2017-11-21 10:23:22', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2017-11-21 10:23:22', '2018-07-02 22:51:09', 'voyager.media.index', NULL),
(3, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 6, '2017-11-21 10:23:22', '2018-07-02 22:51:09', 'voyager.posts.index', NULL),
(4, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 4, '2017-11-21 10:23:22', '2018-07-02 22:51:09', 'voyager.users.index', NULL),
(5, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 8, '2017-11-21 10:23:22', '2018-07-02 22:51:09', 'voyager.categories.index', NULL),
(6, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2017-11-21 10:23:22', '2018-07-02 22:51:09', 'voyager.pages.index', NULL),
(7, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 3, '2017-11-21 10:23:22', '2018-07-02 22:51:09', 'voyager.roles.index', NULL),
(8, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 10, '2017-11-21 10:23:22', '2018-07-02 22:51:03', NULL, NULL),
(9, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 8, 1, '2017-11-21 10:23:22', '2018-05-20 15:08:37', 'voyager.menus.index', NULL),
(10, 1, 'Database', '', '_self', 'voyager-data', NULL, 8, 2, '2017-11-21 10:23:22', '2018-05-20 15:08:37', 'voyager.database.index', NULL),
(11, 1, 'Compass', '/admin/compass', '_self', 'voyager-compass', NULL, 8, 3, '2017-11-21 10:23:22', '2018-05-20 15:08:37', NULL, NULL),
(12, 1, 'Hooks', '/admin/hooks', '_self', 'voyager-hook', '#000000', 8, 5, '2017-11-21 10:23:22', '2018-06-22 14:55:55', NULL, ''),
(13, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 11, '2017-11-21 10:23:22', '2018-07-02 22:51:04', 'voyager.settings.index', NULL),
(14, 1, 'Themes', '/admin/themes', '_self', 'voyager-paint-bucket', NULL, NULL, 12, '2017-11-21 10:31:00', '2018-07-02 22:51:04', NULL, NULL),
(15, 2, 'Dashboard', '', '_self', 'home', '#000000', NULL, 1, '2017-11-28 08:48:21', '2018-03-23 10:25:44', 'wave.dashboard', 'null'),
(16, 2, 'Resources', '#_', '_self', 'info', '#000000', NULL, 2, '2017-11-28 08:49:36', '2017-11-28 09:11:13', NULL, ''),
(19, 2, 'Next Child', '/next', '_self', NULL, '#000000', 18, 1, '2017-11-28 08:56:58', '2017-11-28 08:57:10', NULL, ''),
(20, 2, 'Next Child 2', '/next', '_self', NULL, '#000000', 18, 2, '2017-11-28 08:57:07', '2017-11-28 08:57:12', NULL, ''),
(21, 2, 'Documentation', '/docs', '_self', NULL, '#000000', 16, 1, '2017-11-28 09:08:56', '2017-11-28 09:09:14', NULL, ''),
(22, 2, 'Videos', 'https://devdojo.com/series/wave', '_blank', NULL, '#000000', 16, 2, '2017-11-28 09:09:22', '2017-11-28 09:09:25', NULL, ''),
(23, 2, 'Support', 'https://devdojo.com/forums/category/wave', '_blank', 'lifesaver', '#000000', NULL, 3, '2017-11-28 09:09:56', '2018-03-31 12:22:05', NULL, ''),
(25, 2, 'Blog', '/blog', '_self', NULL, '#000000', 16, 3, '2018-03-31 12:22:02', '2018-03-31 12:22:08', NULL, ''),
(26, 3, 'Home', '/#', '_self', NULL, '#000000', NULL, 99, '2018-04-13 16:29:33', '2018-08-28 12:39:05', NULL, ''),
(27, 3, 'Features', '/#features', '_self', NULL, '#000000', NULL, 100, '2018-04-13 16:30:26', '2018-08-27 18:24:49', NULL, ''),
(28, 3, 'Testimonials', '/#testimonials', '_self', NULL, '#000000', NULL, 101, '2018-04-13 16:31:03', '2018-08-27 18:24:57', NULL, ''),
(29, 3, 'Pricing', '/#pricing', '_self', NULL, '#000000', NULL, 102, '2018-04-13 16:31:52', '2018-08-27 18:25:04', NULL, ''),
(30, 1, 'Announcements', '/admin/announcements', '_self', 'voyager-megaphone', NULL, NULL, 9, '2018-05-20 15:08:14', '2018-07-02 22:51:03', NULL, NULL),
(31, 1, 'BREAD', '', '_self', 'voyager-bread', '#000000', 8, 4, '2018-06-22 14:53:25', '2018-06-22 14:54:13', 'voyager.bread.index', NULL),
(32, 1, 'Plans', '', '_self', 'voyager-logbook', NULL, NULL, 2, '2018-07-02 22:50:28', '2018-07-02 22:51:09', 'voyager.plans.index', NULL),
(33, 3, 'Blog', '', '_self', NULL, '#000000', NULL, 103, '2018-08-24 13:41:14', '2018-08-24 13:41:14', 'wave.blog', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(27, '2018_09_22_234251_add_permissions_group_id_to_permissions_table', 1),
(28, '2018_09_22_234251_add_username_billing_to_users', 1),
(29, '2018_09_22_234251_create_announcement_user_table', 1),
(30, '2018_09_22_234251_create_announcements_table', 1),
(31, '2018_09_22_234251_create_api_keys_table', 1),
(32, '2018_09_22_234251_create_notifications_table', 1),
(33, '2018_09_22_234251_create_permission_groups_table', 1),
(34, '2018_09_22_234251_create_plans_table', 1),
(35, '2018_09_22_234251_create_subscriptions_table', 1),
(36, '2018_09_22_234251_create_voyager_theme_options_table', 1),
(37, '2018_09_22_234251_create_voyager_themes_table', 1),
(38, '2018_09_22_234251_create_wave_key_values_table', 1),
(39, '2018_09_22_234252_add_foreign_keys_to_announcement_user_table', 1),
(40, '2018_09_22_234252_add_foreign_keys_to_plans_table', 1),
(41, '2020_03_30_032031_change_voyager_themes_table_name', 1),
(42, '2020_04_22_234252_add_foreign_keys_to_voyager_theme_options_table', 1),
(43, '2020_06_23_210721_add_stripe_status_column_to_subscriptions_table', 1),
(44, '2020_07_03_000003_create_subscription_items_table', 1),
(45, '2021_01_28_041011_create_paddle_subscriptions_table', 1),
(46, '2021_01_28_182638_removing_cashier_sub_tables', 1),
(47, '2021_01_29_173720_add_slug_column_to_plans_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `paddle_subscriptions`
--

CREATE TABLE `paddle_subscriptions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subscription_id` int(10) UNSIGNED NOT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancel_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelled_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Service', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>&nbsp;</p>\r\n<!-- Section 1 -->\r\n<section class=\"w-full bg-white pt-7 pb-7 md:pt-20 md:pb-24\">\r\n<div class=\"box-border flex flex-col items-center content-center px-8 mx-auto leading-6 text-black border-0 border-gray-300 border-solid md:flex-row max-w-7xl lg:px-16\"><!-- Image -->\r\n<div class=\"box-border relative w-full max-w-md px-4 mt-5 mb-4 -ml-5 text-center bg-no-repeat bg-contain border-solid md:ml-0 md:mt-0 md:max-w-none lg:mb-0 md:w-1/2 xl:pl-10\"><img class=\"p-2 pl-6 pr-5 xl:pl-16 xl:pr-20 \" src=\"https://cdn.devdojo.com/images/december2020/productivity.png\" /></div>\r\n<!-- Content -->\r\n<div class=\"box-border order-first w-full text-black border-solid md:w-1/2 md:pl-10 md:order-none\">\r\n<h2 class=\"m-0 text-xl font-semibold leading-tight border-0 border-gray-300 lg:text-3xl md:text-2xl\">Our Service</h2>\r\n<p class=\"pt-4 pb-8 m-0 leading-7 text-gray-700 border-0 border-gray-300 sm:pr-12 xl:pr-32 lg:text-lg\">Build an atmosphere that creates productivity in your organization and your company culture.</p>\r\n<ul class=\"p-0 m-0 leading-6 border-0 border-gray-300\">\r\n<li class=\"box-border relative py-1 pl-0 text-left text-gray-500 border-solid\"><span class=\"inline-flex items-center justify-center w-6 h-6 mr-2 text-white bg-yellow-300 rounded-full\"><span class=\"text-sm font-bold\">✓</span></span> Maximize productivity and growth</li>\r\n<li class=\"box-border relative py-1 pl-0 text-left text-gray-500 border-solid\"><span class=\"inline-flex items-center justify-center w-6 h-6 mr-2 text-white bg-yellow-300 rounded-full\"><span class=\"text-sm font-bold\">✓</span></span> Speed past your competition</li>\r\n<li class=\"box-border relative py-1 pl-0 text-left text-gray-500 border-solid\"><span class=\"inline-flex items-center justify-center w-6 h-6 mr-2 text-white bg-yellow-300 rounded-full\"><span class=\"text-sm font-bold\">✓</span></span> Learn the top techniques</li>\r\n</ul>\r\n</div>\r\n<!-- End  Content --></div>\r\n<div class=\"box-border flex flex-col items-center content-center px-8 mx-auto mt-2 leading-6 text-black border-0 border-gray-300 border-solid md:mt-20 xl:mt-0 md:flex-row max-w-7xl lg:px-16\"><!-- Content -->\r\n<div class=\"box-border w-full text-black border-solid md:w-1/2 md:pl-6 xl:pl-32\">\r\n<h2 class=\"m-0 text-xl font-semibold leading-tight border-0 border-gray-300 lg:text-3xl md:text-2xl\">Why Choise Us</h2>\r\n<p class=\"pt-4 pb-8 m-0 leading-7 text-gray-700 border-0 border-gray-300 sm:pr-10 lg:text-lg\">Save time and money with our revolutionary services. We are the leaders in the industry.</p>\r\n<ul class=\"p-0 m-0 leading-6 border-0 border-gray-300\">\r\n<li class=\"box-border relative py-1 pl-0 text-left text-gray-500 border-solid\"><span class=\"inline-flex items-center justify-center w-6 h-6 mr-2 text-white bg-yellow-300 rounded-full\"><span class=\"text-sm font-bold\">✓</span></span> Automated task management workflow</li>\r\n<li class=\"box-border relative py-1 pl-0 text-left text-gray-500 border-solid\"><span class=\"inline-flex items-center justify-center w-6 h-6 mr-2 text-white bg-yellow-300 rounded-full\"><span class=\"text-sm font-bold\">✓</span></span> Detailed analytics for your data</li>\r\n<li class=\"box-border relative py-1 pl-0 text-left text-gray-500 border-solid\"><span class=\"inline-flex items-center justify-center w-6 h-6 mr-2 text-white bg-yellow-300 rounded-full\"><span class=\"text-sm font-bold\">✓</span></span> Some awesome integrations</li>\r\n</ul>\r\n</div>\r\n<!-- End  Content --> <!-- Image -->\r\n<div class=\"box-border relative w-full max-w-md px-4 mt-10 mb-4 text-center bg-no-repeat bg-contain border-solid md:mt-0 md:max-w-none lg:mb-0 md:w-1/2\"><img class=\"pl-4 sm:pr-10 xl:pl-10 lg:pr-32\" src=\"https://cdn.devdojo.com/images/december2020/settings.png\" /></div>\r\n</div>\r\n</section>\r\n<!-- Section 2 -->\r\n<section class=\"py-20 bg-white\">\r\n<div class=\"container max-w-6xl mx-auto\">\r\n<h2 class=\"text-4xl font-bold tracking-tight text-center\">Take a look</h2>\r\n<p class=\"mt-2 text-lg text-center text-gray-600\">Check out our list of awesome features below.</p>\r\n<div class=\"grid grid-cols-4 gap-8 mt-10 sm:grid-cols-8 lg:grid-cols-12 sm:px-8 xl:px-0\">\r\n<div class=\"relative flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 overflow-hidden bg-gray-100 sm:rounded-xl\">\r\n<div class=\"p-3 text-white bg-blue-500 rounded-full\">&nbsp;</div>\r\n<h4 class=\"text-xl font-medium text-gray-700\">Certifications</h4>\r\n<p class=\"text-base text-center text-gray-500\">Each of our plan will provide you and your team with certifications.</p>\r\n</div>\r\n<div class=\"flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 bg-gray-100 sm:rounded-xl\">\r\n<div class=\"p-3 text-white bg-blue-500 rounded-full\">&nbsp;</div>\r\n<h4 class=\"text-xl font-medium text-gray-700\">Notifications</h4>\r\n<p class=\"text-base text-center text-gray-500\">Send out notifications to all your customers to keep them engaged.</p>\r\n</div>\r\n<div class=\"flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 bg-gray-100 sm:rounded-xl\">\r\n<div class=\"p-3 text-white bg-blue-500 rounded-full\">&nbsp;</div>\r\n<h4 class=\"text-xl font-medium text-gray-700\">Bundles</h4>\r\n<p class=\"text-base text-center text-gray-500\">High-quality bundles of awesome tools to help you out.</p>\r\n</div>\r\n<div class=\"flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 bg-gray-100 sm:rounded-xl\">\r\n<div class=\"p-3 text-white bg-blue-500 rounded-full\">&nbsp;</div>\r\n<h4 class=\"text-xl font-medium text-gray-700\">Developer Tools</h4>\r\n<p class=\"text-base text-center text-gray-500\">Developer tools to help grow your application and keep it up-to-date.</p>\r\n</div>\r\n<div class=\"flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 bg-gray-100 sm:rounded-xl\">\r\n<div class=\"p-3 text-white bg-blue-500 rounded-full\">&nbsp;</div>\r\n<h4 class=\"text-xl font-medium text-gray-700\">Building Blocks</h4>\r\n<p class=\"text-base text-center text-gray-500\">The right kind of building blocks to take your company to the next level.</p>\r\n</div>\r\n<div class=\"flex flex-col items-center justify-between col-span-4 px-8 py-12 space-y-4 bg-gray-100 sm:rounded-xl\">\r\n<div class=\"p-3 text-white bg-blue-500 rounded-full\">&nbsp;</div>\r\n<h4 class=\"text-xl font-medium text-gray-700\">Coupons</h4>\r\n<p class=\"text-base text-center text-gray-500\">Coupons system to provide special offers and discounts for your app.</p>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<!-- Section 3 -->\r\n<section class=\"py-8 leading-7 text-gray-900 bg-white sm:py-12 md:py-16 lg:py-24\">\r\n<div class=\"max-w-6xl px-4 px-10 mx-auto border-solid lg:px-12\">\r\n<div class=\"flex flex-col items-start leading-7 text-gray-900 border-0 border-gray-200 lg:items-center lg:flex-row\">\r\n<div class=\"box-border flex-1 text-center border-solid sm:text-left\">\r\n<h2 class=\"m-0 text-4xl font-semibold leading-tight tracking-tight text-left text-black border-0 border-gray-200 sm:text-5xl\">Get touch with us</h2>\r\n<p class=\"mt-2 text-xl text-left text-gray-900 border-0 border-gray-200 sm:text-2xl\">Our service will help you maximize and boost your productivity.</p>\r\n</div>\r\n<a class=\"inline-flex items-center justify-center w-full px-5 py-4 mt-6 ml-0 font-sans text-base leading-none text-white no-underline bg-indigo-600 border border-indigo-600 border-solid rounded cursor-pointer md:w-auto lg:mt-0 hover:bg-indigo-700 hover:border-indigo-700 hover:text-white focus-within:bg-indigo-700 focus-within:border-indigo-700 focus-within:text-white sm:text-lg lg:ml-6 md:text-xl\" href=\"#_\"> Get Started </a></div>\r\n</div>\r\n</section>\r\n<!-- Section 4 -->\r\n<section class=\"py-8 leading-7 text-gray-900 bg-white sm:py-12 md:py-16 lg:py-24\">\r\n<div class=\"box-border px-4 mx-auto border-solid sm:px-6 md:px-6 lg:px-8 max-w-7xl\">\r\n<div class=\"flex flex-col items-center leading-7 text-center text-gray-900 border-0 border-gray-200\">\r\n<h2 class=\"box-border m-0 text-3xl font-semibold leading-tight tracking-tight text-black border-solid sm:text-4xl md:text-5xl\">Checkout our pricing</h2>\r\n<p class=\"box-border mt-2 text-xl text-gray-900 border-solid sm:text-2xl\">Pricing to fit the needs of any companie size.</p>\r\n</div>\r\n<div class=\"grid grid-cols-1 gap-4 mt-4 leading-7 text-gray-900 border-0 border-gray-200 sm:mt-6 sm:gap-6 md:mt-8 md:gap-0 lg:grid-cols-3\"><!-- Price 1 -->\r\n<div class=\"relative z-10 flex flex-col items-center max-w-md p-4 mx-auto my-0 border border-solid rounded-lg lg:-mr-3 sm:my-0 sm:p-6 md:my-8 md:p-8\">\r\n<h3 class=\"m-0 text-2xl font-semibold leading-tight tracking-tight text-black border-0 border-gray-200 sm:text-3xl md:text-4xl\">Starter</h3>\r\n<div class=\"flex items-end mt-6 leading-7 text-gray-900 border-0 border-gray-200\">\r\n<p class=\"box-border m-0 text-6xl font-semibold leading-none border-solid\">$5</p>\r\n<p class=\"box-border m-0 border-solid\" style=\"border-image: initial;\">/ month</p>\r\n</div>\r\n<p class=\"mt-6 mb-5 text-base leading-normal text-left text-gray-900 border-0 border-gray-200\">Ideal for Startups and Small Companies</p>\r\n<ul class=\"flex-1 p-0 mt-4 ml-5 leading-7 text-gray-900 border-0 border-gray-200\">\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">Automated Reporting</li>\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">Faster Processing</li>\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">Customizations</li>\r\n</ul>\r\n<button class=\"inline-flex justify-center w-full px-4 py-3 mt-8 font-sans text-sm leading-none text-center text-blue-600 no-underline bg-transparent border border-blue-600 rounded-md cursor-pointer hover:bg-blue-700 hover:border-blue-700 hover:text-white focus-within:bg-blue-700 focus-within:border-blue-700 focus-within:text-white sm:text-base md:text-lg\"> Select Plan </button></div>\r\n<!-- Price 2 -->\r\n<div class=\"relative z-20 flex flex-col items-center max-w-md p-4 mx-auto my-0 bg-white border-4 border-blue-600 border-solid rounded-lg sm:p-6 md:px-8 md:py-16\">\r\n<h3 class=\"m-0 text-2xl font-semibold leading-tight tracking-tight text-black border-0 border-gray-200 sm:text-3xl md:text-4xl\">Basic</h3>\r\n<div class=\"flex items-end mt-6 leading-7 text-gray-900 border-0 border-gray-200\">\r\n<p class=\"box-border m-0 text-6xl font-semibold leading-none border-solid\">$15</p>\r\n<p class=\"box-border m-0 border-solid\" style=\"border-image: initial;\">/ month</p>\r\n</div>\r\n<p class=\"mt-6 mb-5 text-base leading-normal text-left text-gray-900 border-0 border-gray-200\">Ideal for medium-size businesses to larger businesses</p>\r\n<ul class=\"flex-1 p-0 mt-4 leading-7 text-gray-900 border-0 border-gray-200\">\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">Everything in Starter</li>\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">100 Builds</li>\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">Progress Reports</li>\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">Premium Support</li>\r\n</ul>\r\n<button class=\"inline-flex justify-center w-full px-4 py-3 mt-8 font-sans text-sm leading-none text-center text-white no-underline bg-blue-600 border rounded-md cursor-pointer hover:bg-blue-700 hover:border-blue-700 hover:text-white focus-within:bg-blue-700 focus-within:border-blue-700 focus-within:text-white sm:text-base md:text-lg\"> Select Plan </button></div>\r\n<!-- Price 3 -->\r\n<div class=\"relative z-10 flex flex-col items-center max-w-md p-4 mx-auto my-0 border border-solid rounded-lg lg:-ml-3 sm:my-0 sm:p-6 md:my-8 md:p-8\">\r\n<h3 class=\"m-0 text-2xl font-semibold leading-tight tracking-tight text-black border-0 border-gray-200 sm:text-3xl md:text-4xl\">Plus</h3>\r\n<div class=\"flex items-end mt-6 leading-7 text-gray-900 border-0 border-gray-200\">\r\n<p class=\"box-border m-0 text-6xl font-semibold leading-none border-solid\">$25</p>\r\n<p class=\"box-border m-0 border-solid\" style=\"border-image: initial;\">/ month</p>\r\n</div>\r\n<p class=\"mt-6 mb-5 text-base leading-normal text-left text-gray-900 border-0 border-gray-200\">Ideal for larger and enterprise companies</p>\r\n<ul class=\"flex-1 p-0 mt-4 leading-7 text-gray-900 border-0 border-gray-200\">\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">Everything in Basic</li>\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">Unlimited Builds</li>\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">Advanced Analytics</li>\r\n<li class=\"inline-flex items-center block w-full mb-2 ml-5 font-semibold text-left border-solid\">Company Evaluations</li>\r\n</ul>\r\n<button class=\"inline-flex justify-center w-full px-4 py-3 mt-8 font-sans text-sm leading-none text-center text-blue-600 no-underline bg-transparent border border-blue-600 rounded-md cursor-pointer hover:bg-blue-700 hover:border-blue-700 hover:text-white focus-within:bg-blue-700 focus-within:border-blue-700 focus-within:text-white sm:text-base md:text-lg\"> Select Plan </button></div>\r\n</div>\r\n</div>\r\n</section>\r\n<!-- Section 5 -->\r\n<section class=\"relative py-16 bg-white min-w-screen animation-fade animation-delay\">\r\n<div class=\"container px-0 px-8 mx-auto sm:px-12 xl:px-5\">\r\n<p class=\"text-xs font-bold text-left text-pink-500 uppercase sm:mx-6 sm:text-center sm:text-normal sm:font-bold\">Got a Question? We&rsquo;ve got answers.</p>\r\n<h3 class=\"mt-1 text-2xl font-bold text-left text-gray-800 sm:mx-6 sm:text-3xl md:text-4xl lg:text-5xl sm:text-center sm:mx-0\">Frequently Asked Questions</h3>\r\n<div class=\"w-full px-6 py-6 mx-auto mt-10 bg-white border border-gray-200 rounded-lg sm:px-8 md:px-12 sm:py-8 sm:shadow lg:w-5/6 xl:w-2/3\">\r\n<h3 class=\"text-lg font-bold text-purple-500 sm:text-xl md:text-2xl\">How does it work?</h3>\r\n<p class=\"mt-2 text-base text-gray-600 sm:text-lg md:text-normal\">Our platform works with your content to provides insights and metrics on how you can grow your business and scale your infastructure.</p>\r\n</div>\r\n<div class=\"w-full px-6 py-6 mx-auto mt-10 bg-white border border-gray-200 rounded-lg sm:px-8 md:px-12 sm:py-8 sm:shadow lg:w-5/6 xl:w-2/3\">\r\n<h3 class=\"text-lg font-bold text-purple-500 sm:text-xl md:text-2xl\">Do you offer team pricing?</h3>\r\n<p class=\"mt-2 text-base text-gray-600 sm:text-lg md:text-normal\">Yes, we do! Team pricing is available for any plan. You can take advantage of 30% off for signing up for team pricing of 10 users or more.</p>\r\n</div>\r\n<div class=\"w-full px-6 py-6 mx-auto mt-10 bg-white border border-gray-200 rounded-lg sm:px-8 md:px-12 sm:py-8 sm:shadow lg:w-5/6 xl:w-2/3\">\r\n<h3 class=\"text-lg font-bold text-purple-500 sm:text-xl md:text-2xl\">How do I make changes and configure my site?</h3>\r\n<p class=\"mt-2 text-base text-gray-600 sm:text-lg md:text-normal\">You can easily change your site settings inside of your site dashboard by clicking the top right menu and clicking the settings button.</p>\r\n</div>\r\n<div class=\"w-full px-6 py-6 mx-auto mt-10 bg-white border border-gray-200 rounded-lg sm:px-8 md:px-12 sm:py-8 sm:shadow lg:w-5/6 xl:w-2/3\">\r\n<h3 class=\"text-lg font-bold text-purple-500 sm:text-xl md:text-2xl\">How do I add a custom domain?</h3>\r\n<p class=\"mt-2 text-base text-gray-600 sm:text-lg md:text-normal\">You can easily change your site settings inside of your site dashboard by clicking the top right menu and clicking the settings button.</p>\r\n</div>\r\n</div>\r\n</section>', NULL, 'service', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2017-11-21 10:23:23', '2021-04-30 07:24:13'),
(2, 1, 'About', 'This is the about page.', '<section class=\"py-20 bg-gray-50\">\r\n<div class=\"container items-center max-w-6xl px-4 px-10 mx-auto sm:px-20 md:px-32 lg:px-16\">\r\n<div class=\"flex flex-wrap items-center -mx-3\">\r\n<div class=\"order-1 w-full px-3 lg:w-1/2 lg:order-0\">\r\n<div class=\"w-full lg:max-w-md\">\r\n<h2 class=\"mb-4 text-3xl font-bold leading-tight tracking-tight sm:text-4xl font-heading\">Jam-packed with all the tools you need to succeed!</h2>\r\n<p class=\"mb-4 font-medium tracking-tight text-gray-400 xl:mb-6\">It\'s never been easier to build a business of your own. Our tools will help you with the following:</p>\r\n<ul>\r\n<li class=\"flex items-center py-2 space-x-4 xl:py-3\"><span class=\"font-medium text-gray-500\">Faster Processing and Delivery</span></li>\r\n<li class=\"flex items-center py-2 space-x-4 xl:py-3\"><span class=\"font-medium text-gray-500\">Out of the Box Tracking and Monitoring</span></li>\r\n<li class=\"flex items-center py-2 space-x-4 xl:py-3\"><span class=\"font-medium text-gray-500\">100% Protection and Security for Your App</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"w-full px-3 mb-12 lg:w-1/2 order-0 lg:order-1 lg:mb-0\"><img class=\"mx-auto sm:max-w-sm lg:max-w-full\" src=\"https://cdn.devdojo.com/images/november2020/feature-graphic.png\" alt=\"feature image\" /></div>\r\n</div>\r\n</div>\r\n</section>\r\n<section class=\"py-20 bg-gray-50\">\r\n<div class=\"container items-center max-w-6xl px-4 px-10 mx-auto sm:px-20 md:px-32 lg:px-16\">\r\n<div class=\"flex flex-wrap items-center -mx-3\">\r\n<div class=\"order-1 w-full px-3 lg:w-1/2 lg:order-1\">\r\n<div class=\"w-full lg:max-w-md\">\r\n<h2 class=\"mb-4 text-3xl font-bold leading-tight tracking-tight sm:text-4xl font-heading\">Jam-packed with all the tools you need to succeed!</h2>\r\n<p class=\"mb-4 font-medium tracking-tight text-gray-400 xl:mb-6\">It\'s never been easier to build a business of your own. Our tools will help you with the following:</p>\r\n<ul>\r\n<li class=\"flex items-center py-2 space-x-4 xl:py-3\"><span class=\"font-medium text-gray-500\">Faster Processing and Delivery</span></li>\r\n<li class=\"flex items-center py-2 space-x-4 xl:py-3\"><span class=\"font-medium text-gray-500\">Out of the Box Tracking and Monitoring</span></li>\r\n<li class=\"flex items-center py-2 space-x-4 xl:py-3\"><span class=\"font-medium text-gray-500\">100% Protection and Security for Your App</span></li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div class=\"w-full px-3 mb-12 lg:w-1/2 order-0 lg:order-0 lg:mb-0\"><img class=\"mx-auto sm:max-w-sm lg:max-w-full\" src=\"https://cdn.devdojo.com/images/november2020/feature-graphic.png\" alt=\"feature image\" /></div>\r\n</div>\r\n</div>\r\n</section>\r\n<!-- Section 3 -->\r\n<section class=\"bg-white pt-7 pb-14\">\r\n<div class=\"container px-8 mx-auto sm:px-12 lg:px-20\">\r\n<h1 class=\"text-sm font-bold tracking-wide text-center text-gray-800 uppercase mb-7\">Trusted by top-leading companies.</h1>\r\n<div class=\"flex grid items-center justify-center grid-cols-4 grid-cols-12 gap-y-8\">\r\n<div class=\"flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-3 xl:col-span-2\"><img class=\"block object-contain h-12\" src=\"https://cdn.devdojo.com/tails/images/disney-plus.svg\" alt=\"Disney Plus\" /></div>\r\n<div class=\"flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-3 xl:col-span-2\"><img class=\"block object-contain h-9\" src=\"https://cdn.devdojo.com/tails/images/google.svg\" alt=\"Google\" /></div>\r\n<div class=\"flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-3 xl:col-span-2\"><img class=\"block object-contain h-9\" src=\"https://cdn.devdojo.com/tails/images/hubspot.svg\" alt=\"Hubspot\" /></div>\r\n<div class=\"flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-3 xl:col-span-2\"><img class=\"block object-contain h-7 lg:h-8\" src=\"https://cdn.devdojo.com/tails/images/youtube.svg\" alt=\"Youtube\" /></div>\r\n<div class=\"flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-6 xl:col-span-2\"><img class=\"block object-contain h-9\" src=\"https://cdn.devdojo.com/tails/images/slack.svg\" alt=\"Slack\" /></div>\r\n<div class=\"flex items-center justify-center col-span-6 sm:col-span-4 md:col-span-6 xl:col-span-2\"><img class=\"block object-contain h-9\" src=\"https://cdn.devdojo.com/tails/images/shopify.svg\" alt=\"Shopify\" /></div>\r\n</div>\r\n</div>\r\n</section>\r\n<!-- Section 4 -->\r\n<section class=\"py-8 leading-7 text-gray-900 bg-white sm:py-12 md:py-16 lg:py-24\">\r\n<div class=\"max-w-6xl px-4 px-10 mx-auto border-solid lg:px-12\">\r\n<div class=\"flex flex-col items-start leading-7 text-gray-900 border-0 border-gray-200 lg:items-center lg:flex-row\">\r\n<div class=\"box-border flex-1 text-center border-solid sm:text-left\">\r\n<h2 class=\"m-0 text-4xl font-semibold leading-tight tracking-tight text-left text-black border-0 border-gray-200 sm:text-5xl\">Boost Your Productivity</h2>\r\n<p class=\"mt-2 text-xl text-left text-gray-900 border-0 border-gray-200 sm:text-2xl\">Our service will help you maximize and boost your productivity.</p>\r\n</div>\r\n<a class=\"inline-flex items-center justify-center w-full px-5 py-4 mt-6 ml-0 font-sans text-base leading-none text-white no-underline bg-indigo-600 border border-indigo-600 border-solid rounded cursor-pointer md:w-auto lg:mt-0 hover:bg-indigo-700 hover:border-indigo-700 hover:text-white focus-within:bg-indigo-700 focus-within:border-indigo-700 focus-within:text-white sm:text-lg lg:ml-6 md:text-xl\" href=\"#_\"> Get Started </a></div>\r\n</div>\r\n</section>', NULL, 'about', 'About Wave', 'about, wave', 'ACTIVE', '2018-03-29 21:04:51', '2021-04-30 04:33:22'),
(3, 1, 'Contact Us', 'dd', '<p>&nbsp;</p>\r\n<!-- Section 1 -->\r\n<section class=\"px-2 py-32 bg-white md:px-0\">\r\n<div class=\"container items-center max-w-6xl px-8 mx-auto xl:px-5\">\r\n<div class=\"flex flex-wrap items-center sm:-mx-3\">\r\n<div class=\"w-full md:w-1/2 md:px-3\">\r\n<div class=\"w-full pb-6 space-y-6 sm:max-w-md lg:max-w-lg md:space-y-4 lg:space-y-8 xl:space-y-9 sm:pr-5 lg:pr-0 md:pb-0\">\r\n<h1 class=\"text-4xl font-extrabold tracking-tight text-gray-900 sm:text-5xl md:text-4xl lg:text-5xl xl:text-6xl\"><span class=\"block xl:inline\">Beautiful Pages to</span> <span class=\"block text-indigo-600 xl:inline\">Tell Your Story!</span></h1>\r\n<p class=\"mx-auto text-base text-gray-500 sm:max-w-md lg:text-xl md:max-w-3xl\">It\'s never been easier to build beautiful websites that convey your message and tell your story.</p>\r\n<div class=\"relative flex flex-col sm:flex-row sm:space-x-4\"><a class=\"flex items-center w-full px-6 py-3 mb-3 text-lg text-white bg-indigo-600 rounded-md sm:mb-0 hover:bg-indigo-700 sm:w-auto\" href=\"#_\"> Try It Free </a> <a class=\"flex items-center px-6 py-3 text-gray-500 bg-gray-100 rounded-md hover:bg-gray-200 hover:text-gray-600\" href=\"#_\"> Learn More </a></div>\r\n</div>\r\n</div>\r\n<div class=\"w-full md:w-1/2\">\r\n<div class=\"w-full h-auto overflow-hidden rounded-md shadow-xl sm:rounded-xl\"><img class=\"\" src=\"https://cdn.devdojo.com/images/november2020/hero-image.jpeg\" /></div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>\r\n<!-- Section 2 -->\r\n<section class=\"w-full bg-white\">\r\n<div class=\"mx-auto max-w-7xl\">\r\n<div class=\"flex flex-col lg:flex-row\">\r\n<div class=\"relative w-full bg-cover lg:w-6/12 xl:w-7/12 bg-gradient-to-r from-white via-white to-gray-100\">\r\n<div class=\"relative flex flex-col items-center justify-center w-full h-full px-10 my-20 lg:px-16 lg:my-0\">\r\n<div class=\"flex flex-col items-start space-y-8 tracking-tight lg:max-w-3xl\">\r\n<div class=\"relative\">\r\n<p class=\"mb-2 font-medium text-gray-700 uppercase\">Work smarter</p>\r\n<h2 class=\"text-5xl font-bold text-gray-900 xl:text-6xl\">Features to help you work smarter</h2>\r\n</div>\r\n<p class=\"text-2xl text-gray-700\">We\'ve created a simple formula to follow in order to gain more out of your business and your application.</p>\r\n<a class=\"inline-block px-8 py-5 text-xl font-medium text-center text-white transition duration-200 bg-blue-600 rounded-lg hover:bg-blue-700 ease\" href=\"#_\">Get Started Today</a></div>\r\n</div>\r\n</div>\r\n<div class=\"w-full bg-white lg:w-6/12 xl:w-5/12\">\r\n<div class=\"flex flex-col items-start justify-start w-full h-full p-10 lg:p-16 xl:p-24\">\r\n<h4 class=\"w-full text-3xl font-bold\">Signup</h4>\r\n<p class=\"text-lg text-gray-500\">or, if you have an account you can <a class=\"text-blue-600 underline\" href=\"#_\">sign in</a></p>\r\n<div class=\"relative w-full mt-10 space-y-8\">\r\n<div class=\"relative\"><label class=\"font-medium text-gray-900\">Name</label> <input type=\"text\" class=\"block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50\" placeholder=\"Enter Your Name\" /></div>\r\n<div class=\"relative\"><label class=\"font-medium text-gray-900\">Email</label> <input type=\"text\" class=\"block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50\" placeholder=\"Enter Your Email Address\" /></div>\r\n<div class=\"relative\"><label class=\"font-medium text-gray-900\">Password</label> <input type=\"password\" class=\"block w-full px-4 py-4 mt-2 text-xl placeholder-gray-400 bg-gray-200 rounded-lg focus:outline-none focus:ring-4 focus:ring-blue-600 focus:ring-opacity-50\" placeholder=\"Password\" /></div>\r\n<div class=\"relative\"><a class=\"inline-block w-full px-5 py-4 text-lg font-medium text-center text-white transition duration-200 bg-blue-600 rounded-lg hover:bg-blue-700 ease\" href=\"#_\">Create Account</a> <a class=\"inline-block w-full px-5 py-4 mt-3 text-lg font-bold text-center text-gray-900 transition duration-200 bg-white border border-gray-300 rounded-lg hover:bg-gray-100 ease\" href=\"#_\">Sign up with Google</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</section>', NULL, 'contact-us', 'ddd', 'ddd', 'ACTIVE', '2021-04-30 07:28:52', '2021-04-30 07:28:52');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`, `permission_group_id`) VALUES
(1, 'browse_admin', NULL, '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(2, 'browse_bread', NULL, '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(3, 'browse_database', NULL, '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(4, 'browse_media', NULL, '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(5, 'browse_compass', NULL, '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(6, 'browse_menus', 'menus', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(7, 'read_menus', 'menus', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(8, 'edit_menus', 'menus', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(9, 'add_menus', 'menus', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(10, 'delete_menus', 'menus', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(11, 'browse_roles', 'roles', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(12, 'read_roles', 'roles', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(13, 'edit_roles', 'roles', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(14, 'add_roles', 'roles', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(15, 'delete_roles', 'roles', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(16, 'browse_users', 'users', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(17, 'read_users', 'users', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(18, 'edit_users', 'users', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(19, 'add_users', 'users', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(20, 'delete_users', 'users', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(21, 'browse_settings', 'settings', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(22, 'read_settings', 'settings', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(23, 'edit_settings', 'settings', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(24, 'add_settings', 'settings', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(25, 'delete_settings', 'settings', '2018-06-22 14:15:45', '2018-06-22 14:15:45', NULL),
(26, 'browse_categories', 'categories', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(27, 'read_categories', 'categories', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(28, 'edit_categories', 'categories', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(29, 'add_categories', 'categories', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(30, 'delete_categories', 'categories', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(31, 'browse_posts', 'posts', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(32, 'read_posts', 'posts', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(33, 'edit_posts', 'posts', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(34, 'add_posts', 'posts', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(35, 'delete_posts', 'posts', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(36, 'browse_pages', 'pages', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(37, 'read_pages', 'pages', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(38, 'edit_pages', 'pages', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(39, 'add_pages', 'pages', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(40, 'delete_pages', 'pages', '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(41, 'browse_hooks', NULL, '2018-06-22 14:15:46', '2018-06-22 14:15:46', NULL),
(42, 'browse_announcements', 'announcements', '2018-05-20 15:08:14', '2018-05-20 15:08:14', NULL),
(43, 'read_announcements', 'announcements', '2018-05-20 15:08:14', '2018-05-20 15:08:14', NULL),
(44, 'edit_announcements', 'announcements', '2018-05-20 15:08:14', '2018-05-20 15:08:14', NULL),
(45, 'add_announcements', 'announcements', '2018-05-20 15:08:14', '2018-05-20 15:08:14', NULL),
(46, 'delete_announcements', 'announcements', '2018-05-20 15:08:14', '2018-05-20 15:08:14', NULL),
(47, 'browse_themes', 'admin', '2017-11-21 10:31:00', '2017-11-21 10:31:00', NULL),
(48, 'browse_hooks', 'hooks', '2018-06-22 07:55:03', '2018-06-22 07:55:03', NULL),
(49, 'read_hooks', 'hooks', '2018-06-22 07:55:03', '2018-06-22 07:55:03', NULL),
(50, 'edit_hooks', 'hooks', '2018-06-22 07:55:03', '2018-06-22 07:55:03', NULL),
(51, 'add_hooks', 'hooks', '2018-06-22 07:55:03', '2018-06-22 07:55:03', NULL),
(52, 'delete_hooks', 'hooks', '2018-06-22 07:55:03', '2018-06-22 07:55:03', NULL),
(53, 'browse_plans', 'plans', '2018-07-02 22:50:28', '2018-07-02 22:50:28', NULL),
(54, 'read_plans', 'plans', '2018-07-02 22:50:28', '2018-07-02 22:50:28', NULL),
(55, 'edit_plans', 'plans', '2018-07-02 22:50:28', '2018-07-02 22:50:28', NULL),
(56, 'add_plans', 'plans', '2018-07-02 22:50:28', '2018-07-02 22:50:28', NULL),
(57, 'delete_plans', 'plans', '2018-07-02 22:50:28', '2018-07-02 22:50:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_groups`
--

CREATE TABLE `permission_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(7, 5),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(16, 3),
(16, 4),
(16, 5),
(17, 1),
(17, 3),
(17, 4),
(17, 5),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(26, 2),
(26, 3),
(26, 4),
(26, 5),
(27, 1),
(27, 2),
(27, 3),
(27, 4),
(27, 5),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(31, 2),
(31, 3),
(31, 4),
(31, 5),
(32, 1),
(32, 2),
(32, 3),
(32, 4),
(32, 5),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(36, 2),
(36, 3),
(36, 4),
(36, 5),
(37, 1),
(37, 2),
(37, 3),
(37, 4),
(37, 5),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(42, 2),
(42, 3),
(42, 4),
(42, 5),
(43, 1),
(43, 2),
(43, 3),
(43, 4),
(43, 5),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1);

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `features` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plan_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trial_days` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `name`, `slug`, `description`, `features`, `plan_id`, `role_id`, `default`, `price`, `trial_days`, `created_at`, `updated_at`) VALUES
(1, 'Basic', 'basic', 'Signup for the Basic User Plan to access all the basic features.', 'Basic Feature Example 1, Basic Feature Example 2, Basic Feature Example 3, Basic Feature Example 4', '1', 3, 0, '5', 0, '2018-07-02 23:03:56', '2018-07-03 11:17:24'),
(2, 'Premium', 'premium', 'Signup for our premium plan to access all our Premium Features.', 'Premium Feature Example 1, Premium Feature Example 2, Premium Feature Example 3, Premium Feature Example 4', '2', 5, 1, '8', 0, '2018-07-03 10:29:46', '2018-07-03 11:17:08'),
(3, 'Pro', 'pro', 'Gain access to our pro features with the pro plan.', 'Pro Feature Example 1, Pro Feature Example 2, Pro Feature Example 3, Pro Feature Example 4', '3', 4, 0, '12', 14, '2018-07-03 10:30:43', '2018-08-22 16:26:19');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(5, 1, 1, 'Best ways to market your application', 'Best ways to market your application', NULL, '<p>There are many different ways to market your application. First, let\'s start off at the beginning and then we will get more in-depth. You\'ll want to discover your target audience and after that, you\'ll want to run some ads.</p>\n<p>Let\'s not complicate things here, if you build a good product, you are going to have users. But you will need to let your users know where to find you. This is where social media and ads come in to play. You\'ll need to boast about your product and your app. If it\'s something that you really believe in, the odds are others will too.</p>\n<blockquote>\n<p>You may have a need to only want to make money from your application, but if your application can help others achieve a goal and you can make money from it too, you have a gold-mine.</p>\n</blockquote>\n<p>Some more info on your awesome post here. After this sentence, it\'s just going to be a little bit of jibberish. But you get a general idea. You\'ll want to blog about stuff to get your customers interested in your application. With leverage existing reliable initiatives before leveraged ideas. Rapidiously develops equity invested expertise rather than enabled channels. Monotonectally intermediate distinctive networks before highly efficient core competencies.</p>\n<h2>Seamlessly promote flexible growth strategies.</h2>\n<p><img src=\"/storage/posts/March2018/blog-1.jpg\" alt=\"blog\" /></p><p> Dramatically harness extensive value through the fully researched human capital. Seamlessly transition premium schemas vis-a-vis efficient convergence. Intrinsically build competitive e-commerce with cross-unit information. Collaboratively e-enable real-time processes before extensive technology. Authoritatively fabricate efficient metrics through intuitive quality vectors.</p>\n<p>Collaboratively deliver optimal vortals whereas backward-compatible models. Globally syndicate diverse leadership rather than high-payoff experiences. Uniquely pontificate unique metrics for cross-media human capital. Completely procrastinate professional collaboration and idea-sharing rather than 24/365 paradigms. Phosfluorescently initiates multimedia based outsourcing for interoperable benefits.</p>\n<h3>Seamlessly promote flexible growth strategies.</h3>\n<p>Progressively leverage other\'s e-business functionalities through corporate e-markets. Holistic repurpose timely systems via seamless total linkage. Appropriately maximize impactful \"outside the box\" thinking vis-a-vis visionary value. Authoritatively deploy interdependent technology through process-centric \"outside the box\" thinking. Interactively negotiate pandemic internal or \"organic\" sources whereas competitive relationships.</p>\n<figure><img src=\"/storage/posts/March2018/blog-2.jpg\" alt=\"wide\" />\n<figcaption>Keep working until you find success.</figcaption>\n</figure>\n<p>Enthusiastically deliver viral potentialities through multidisciplinary products. Synergistically plagiarize client-focused partnerships for adaptive applications. Seamlessly morph process-centric synergy whereas bricks-and-clicks deliverables. Continually disintermediate holistic action items without distinctive customer service. Enthusiastically seize enterprise web-readiness without effective schemas.</p>\n<h4>Seamlessly promote flexible growth strategies.</h4>\n<p>Assertively restore installed base data before sustainable platforms. Globally recapitalize orthogonal systems via clicks-and-mortar web services. Efficiently grow visionary action items through collaborative e-commerce. Efficiently architect highly efficient \"outside the box\" thinking before customer directed infomediaries. Proactively mesh holistic human capital rather than exceptional niches.</p>\n<p>Intrinsically create innovative value and pandemic resources. Progressively productize turnkey e-markets and economically sound synergy. Objectively supply turnkey imperatives vis-a-vis high standards in outsourcing. Dynamically exploit unique imperatives with dynamic systems. Appropriately formulate technically sound users and excellent expertise.</p>\n<p>Competently redefine long-term high-impact relationships rather than effective metrics. Distinctively maintain impactful platforms after strategic imperatives. Intrinsically evolve mission-critical deliverables after multimedia based e-business. Interactively mesh cooperative benefits whereas distributed process improvements. Progressively monetize an expanded array of e-services whereas.</p>', 'posts/March2018/h86hSqPMkT9oU8pjcrSu.jpg', 'best-ways-to-market-your-application', 'Find out the best ways to market your application in this article.', 'market, saas, market your app', 'PUBLISHED', 0, '2018-03-25 20:55:01', '2018-03-25 20:13:05'),
(6, 1, 1, 'Achieving your Dreams', 'Achieving your Dreams', NULL, '<p>What can be said about achieving your dreams? <br>Well... It\'s a good thing, and it\'s probably something you\'re dreaming of. Oh yeah, when you create an app and a product that you enjoy working on... You\'ll be pretty happy and your dreams will probably come true. Cool, right?</p>\n<p>I hope that you are ready for some cool stuff because there is some cool stuff right around the corner. By the time you\'ve reached the sky, you\'ll realize your true limits. That last sentence there... That was a little bit of jibberish, but I\'m trying to write about something cool. Bottom line is that Wave is going to help save you so much time.</p>\n<blockquote>\n<p>You may have a need to only want to make money from your application, but if your application can help others achieve a goal and you can make money from it too, you have a gold-mine.</p>\n</blockquote>\n<p>Some more info on your awesome post here. After this sentence, it\'s just going to be a little bit of jibberish. But you get a general idea. You\'ll want to blog about stuff to get your customers interested in your application. With leverage existing reliable initiatives before leveraged ideas. Rapidiously develops equity invested expertise rather than enabled channels. Monotonectally intermediate distinctive networks before highly efficient core competencies.</p>\n<h2>Seamlessly promote flexible growth strategies.</h2>\n<p><img src=\"/storage/posts/March2018/blog-1.jpg\" alt=\"blog\" /></p><p>Dramatically harness extensive value through the fully researched human capital. Seamlessly transition premium schemas vis-a-vis efficient convergence. Intrinsically build competitive e-commerce with cross-unit information. Collaboratively e-enable real-time processes before extensive technology. Authoritatively fabricate efficient metrics through intuitive quality vectors.</p>\n<p>Collaboratively deliver optimal vortals whereas backward-compatible models. Globally syndicate diverse leadership rather than high-payoff experiences. Uniquely pontificate unique metrics for cross-media human capital. Completely procrastinate professional collaboration and idea-sharing rather than 24/365 paradigms. Phosfluorescently initiates multimedia based outsourcing for interoperable benefits.</p>\n<h3>Seamlessly promote flexible growth strategies.</h3>\n<p>Progressively leverage other\'s e-business functionalities through corporate e-markets. Holistic repurpose timely systems via seamless total linkage. Appropriately maximize impactful \"outside the box\" thinking vis-a-vis visionary value. Authoritatively deploy interdependent technology through process-centric \"outside the box\" thinking. Interactively negotiate pandemic internal or \"organic\" sources whereas competitive relationships.</p>\n<figure><img src=\"/storage/posts/March2018/blog-2.jpg\" alt=\"wide\" />\n<figcaption>Keep working until you find success.</figcaption>\n</figure>\n<p>Enthusiastically deliver viral potentialities through multidisciplinary products. Synergistically plagiarize client-focused partnerships for adaptive applications. Seamlessly morph process-centric synergy whereas bricks-and-clicks deliverables. Continually disintermediate holistic action items without distinctive customer service. Enthusiastically seize enterprise web-readiness without effective schemas.</p>\n<h4>Seamlessly promote flexible growth strategies.</h4>\n<p>Assertively restore installed base data before sustainable platforms. Globally recapitalize orthogonal systems via clicks-and-mortar web services. Efficiently grow visionary action items through collaborative e-commerce. Efficiently architect highly efficient \"outside the box\" thinking before customer directed infomediaries. Proactively mesh holistic human capital rather than exceptional niches.</p>\n<p>Intrinsically create innovative value and pandemic resources. Progressively productize turnkey e-markets and economically sound synergy. Objectively supply turnkey imperatives vis-a-vis high standards in outsourcing. Dynamically exploit unique imperatives with dynamic systems. Appropriately formulate technically sound users and excellent expertise.</p>\n<p>Competently redefine long-term high-impact relationships rather than effective metrics. Distinctively maintain impactful platforms after strategic imperatives. Intrinsically evolve mission-critical deliverables after multimedia based e-business. Interactively mesh cooperative benefits whereas distributed process improvements. Progressively monetize an expanded array of e-services whereas.</p>', 'posts/March2018/rU26aWVsZ2zocWGSTE7J.jpg', 'achieving-your-dreams', 'In this post, you\'ll learn about achieving your dreams by building the SAAS app of your dreams', 'saas app, dreams', 'PUBLISHED', 0, '2018-03-25 20:50:18', '2018-03-25 20:15:18'),
(7, 1, 1, 'Building a solid foundation', 'Building a solid foundation', NULL, '<p>The foundation is one of the most important aspects. You\'ll want to make sure that you build your application on a solid foundation because this is where every other feature will grow on top of.</p>\n<p>If the foundation is unstable the rest of the application will be so as well. But a solid foundation will make mediocre features seem amazing. So, if you want to save yourself some time you will build your application on a solid foundation of cool features, awesome jumps, and killer waves... I don\'t know what this paragraph is about anymore.</p>\n<blockquote>\n<p>You may have a need to only want to make money from your application, but if your application can help others achieve a goal and you can make money from it too, you have a gold-mine.</p>\n</blockquote>\n<p>Some more info on your awesome post here. After this sentence, it\'s just going to be a little bit of jibberish. But you get a general idea. You\'ll want to blog about stuff to get your customers interested in your application. With leverage existing reliable initiatives before leveraged ideas. Rapidiously develops equity invested expertise rather than enabled channels. Monotonectally intermediate distinctive networks before highly efficient core competencies.</p>\n<h2>Seamlessly promote flexible growth strategies.</h2>\n<p><img src=\"/storage/posts/March2018/blog-1.jpg\" alt=\"blog\" /></p><p>Dramatically harness extensive value through the fully researched human capital. Seamlessly transition premium schemas vis-a-vis efficient convergence. Intrinsically build competitive e-commerce with cross-unit information. Collaboratively e-enable real-time processes before extensive technology. Authoritatively fabricate efficient metrics through intuitive quality vectors.</p>\n<p>Collaboratively deliver optimal vortals whereas backward-compatible models. Globally syndicate diverse leadership rather than high-payoff experiences. Uniquely pontificate unique metrics for cross-media human capital. Completely procrastinate professional collaboration and idea-sharing rather than 24/365 paradigms. Phosfluorescently initiates multimedia based outsourcing for interoperable benefits.</p>\n<h3>Seamlessly promote flexible growth strategies.</h3>\n<p>Progressively leverage other\'s e-business functionalities through corporate e-markets. Holistic repurpose timely systems via seamless total linkage. Appropriately maximize impactful \"outside the box\" thinking vis-a-vis visionary value. Authoritatively deploy interdependent technology through process-centric \"outside the box\" thinking. Interactively negotiate pandemic internal or \"organic\" sources whereas competitive relationships.</p>\n<figure><img src=\"/storage/posts/March2018/blog-2.jpg\" alt=\"wide\" />\n<figcaption>Keep working until you find success.</figcaption>\n</figure>\n<p>Enthusiastically deliver viral potentialities through multidisciplinary products. Synergistically plagiarize client-focused partnerships for adaptive applications. Seamlessly morph process-centric synergy whereas bricks-and-clicks deliverables. Continually disintermediate holistic action items without distinctive customer service. Enthusiastically seize enterprise web-readiness without effective schemas.</p>\n<h4>Seamlessly promote flexible growth strategies.</h4>\n<p>Assertively restore installed base data before sustainable platforms. Globally recapitalize orthogonal systems via clicks-and-mortar web services. Efficiently grow visionary action items through collaborative e-commerce. Efficiently architect highly efficient \"outside the box\" thinking before customer directed infomediaries. Proactively mesh holistic human capital rather than exceptional niches.</p>\n<p>Intrinsically create innovative value and pandemic resources. Progressively productize turnkey e-markets and economically sound synergy. Objectively supply turnkey imperatives vis-a-vis high standards in outsourcing. Dynamically exploit unique imperatives with dynamic systems. Appropriately formulate technically sound users and excellent expertise.</p>\n<p>Competently redefine long-term high-impact relationships rather than effective metrics. Distinctively maintain impactful platforms after strategic imperatives. Intrinsically evolve mission-critical deliverables after multimedia based e-business. Interactively mesh cooperative benefits whereas distributed process improvements. Progressively monetize an expanded array of e-services whereas.&nbsp;</p>', 'posts/March2018/4vI1gzsAvMZ30yfDIe67.jpg', 'building-a-solid-foundation', 'Building a solid foundation for your application is super important. Read on below.', 'foundation, app foundation', 'PUBLISHED', 0, '2018-03-25 20:24:43', '2018-03-25 20:24:43'),
(8, 1, 2, 'Finding the solution that fits for you', 'Finding the solution that fits for you', NULL, '<p>There is a fit for each person. Depending on the service you may want to focus on what each person needs. When you find this you\'ll be able to segregate your application to fit each person\'s needs.</p>\n<p>This is really just an example post. I could write some stuff about how this and that, but it would probably only be information about this and that. Who am I kidding? This really isn\'t going to make some sense, but thanks for still reading. Are you still reading this article? That\'s awesome. Thanks for being interested.</p>\n<blockquote>\n<p>You may have a need to only want to make money from your application, but if your application can help others achieve a goal and you can make money from it too, you have a gold-mine.</p>\n</blockquote>\n<p>Some more info on your awesome post here. After this sentence, it\'s just going to be a little bit of jibberish. But you get a general idea. You\'ll want to blog about stuff to get your customers interested in your application. With leverage existing reliable initiatives before leveraged ideas. Rapidiously develops equity invested expertise rather than enabled channels. Monotonectally intermediate distinctive networks before highly efficient core competencies.</p>\n<h2>Seamlessly promote flexible growth strategies.</h2>\n<p><img src=\"/storage/posts/March2018/blog-1.jpg\" alt=\"blog\" /></p><p>Dramatically harness extensive value through the fully researched human capital. Seamlessly transition premium schemas vis-a-vis efficient convergence. Intrinsically build competitive e-commerce with cross-unit information. Collaboratively e-enable real-time processes before extensive technology. Authoritatively fabricate efficient metrics through intuitive quality vectors.</p>\n<p>Collaboratively deliver optimal vortals whereas backward-compatible models. Globally syndicate diverse leadership rather than high-payoff experiences. Uniquely pontificate unique metrics for cross-media human capital. Completely procrastinate professional collaboration and idea-sharing rather than 24/365 paradigms. Phosfluorescently initiates multimedia based outsourcing for interoperable benefits.</p>\n<h3>Seamlessly promote flexible growth strategies.</h3>\n<p>Progressively leverage other\'s e-business functionalities through corporate e-markets. Holistic repurpose timely systems via seamless total linkage. Appropriately maximize impactful \"outside the box\" thinking vis-a-vis visionary value. Authoritatively deploy interdependent technology through process-centric \"outside the box\" thinking. Interactively negotiate pandemic internal or \"organic\" sources whereas competitive relationships.</p>\n<figure><img src=\"/storage/posts/March2018/blog-2.jpg\" alt=\"wide\" />\n<figcaption>Keep working until you find success.</figcaption>\n</figure>\n<p>Enthusiastically deliver viral potentialities through multidisciplinary products. Synergistically plagiarize client-focused partnerships for adaptive applications. Seamlessly morph process-centric synergy whereas bricks-and-clicks deliverables. Continually disintermediate holistic action items without distinctive customer service. Enthusiastically seize enterprise web-readiness without effective schemas.</p>\n<h4>Seamlessly promote flexible growth strategies.</h4>\n<p>Assertively restore installed base data before sustainable platforms. Globally recapitalize orthogonal systems via clicks-and-mortar web services. Efficiently grow visionary action items through collaborative e-commerce. Efficiently architect highly efficient \"outside the box\" thinking before customer directed infomediaries. Proactively mesh holistic human capital rather than exceptional niches.</p>\n<p>Intrinsically create innovative value and pandemic resources. Progressively productize turnkey e-markets and economically sound synergy. Objectively supply turnkey imperatives vis-a-vis high standards in outsourcing. Dynamically exploit unique imperatives with dynamic systems. Appropriately formulate technically sound users and excellent expertise.</p>\n<p>Competently redefine long-term high-impact relationships rather than effective metrics. Distinctively maintain impactful platforms after strategic imperatives. Intrinsically evolve mission-critical deliverables after multimedia based e-business. Interactively mesh cooperative benefits whereas distributed process improvements. Progressively monetize an expanded array of e-services whereas.&nbsp;</p>', 'posts/March2018/hWOT5yqNmzCnLhVWXB2u.jpg', 'finding-the-solution-that-fits-for-you', 'How to build an app and find a solution that fits each users needs', 'solution, app solution', 'PUBLISHED', 0, '2018-03-25 20:42:44', '2018-03-25 20:42:44'),
(9, 1, 2, 'Creating something useful', 'Creating something useful', NULL, '<p>It\'s not enough nowadays to create something you want, instead you\'ll need to focus on what people need. If you find a need for something that isn\'t available... You should create it. Odds are someone will find it useful as well.</p>\n<p>When you focus your energy on building something that you are passionate about it\'s going to show. Your customers will buy because it\'s a great application, but also because they believe in what you are trying to achieve. So, continue to focus on making something that people need and find useful.</p>\n<blockquote>\n<p>You may have a need to only want to make money from your application, but if your application can help others achieve a goal and you can make money from it too, you have a gold-mine.</p>\n</blockquote>\n<p>Some more info on your awesome post here. After this sentence, it\'s just going to be a little bit of jibberish. But you get a general idea. You\'ll want to blog about stuff to get your customers interested in your application. With leverage existing reliable initiatives before leveraged ideas. Rapidiously develops equity invested expertise rather than enabled channels. Monotonectally intermediate distinctive networks before highly efficient core competencies.</p>\n<h2>Seamlessly promote flexible growth strategies.</h2>\n<p><img src=\"/storage/posts/March2018/blog-1.jpg\" alt=\"blog\" /></p><p>Dramatically harness extensive value through the fully researched human capital. Seamlessly transition premium schemas vis-a-vis efficient convergence. Intrinsically build competitive e-commerce with cross-unit information. Collaboratively e-enable real-time processes before extensive technology. Authoritatively fabricate efficient metrics through intuitive quality vectors.</p>\n<p>Collaboratively deliver optimal vortals whereas backward-compatible models. Globally syndicate diverse leadership rather than high-payoff experiences. Uniquely pontificate unique metrics for cross-media human capital. Completely procrastinate professional collaboration and idea-sharing rather than 24/365 paradigms. Phosfluorescently initiates multimedia based outsourcing for interoperable benefits.</p>\n<h3>Seamlessly promote flexible growth strategies.</h3>\n<p>Progressively leverage other\'s e-business functionalities through corporate e-markets. Holistic repurpose timely systems via seamless total linkage. Appropriately maximize impactful \"outside the box\" thinking vis-a-vis visionary value. Authoritatively deploy interdependent technology through process-centric \"outside the box\" thinking. Interactively negotiate pandemic internal or \"organic\" sources whereas competitive relationships.</p>\n<figure><img src=\"/storage/posts/March2018/blog-2.jpg\" alt=\"wide\" />\n<figcaption>Keep working until you find success.</figcaption>\n</figure>\n<p>Enthusiastically deliver viral potentialities through multidisciplinary products. Synergistically plagiarize client-focused partnerships for adaptive applications. Seamlessly morph process-centric synergy whereas bricks-and-clicks deliverables. Continually disintermediate holistic action items without distinctive customer service. Enthusiastically seize enterprise web-readiness without effective schemas.</p>\n<h4>Seamlessly promote flexible growth strategies.</h4>\n<p>Assertively restore installed base data before sustainable platforms. Globally recapitalize orthogonal systems via clicks-and-mortar web services. Efficiently grow visionary action items through collaborative e-commerce. Efficiently architect highly efficient \"outside the box\" thinking before customer directed infomediaries. Proactively mesh holistic human capital rather than exceptional niches.</p>\n<p>Intrinsically create innovative value and pandemic resources. Progressively productize turnkey e-markets and economically sound synergy. Objectively supply turnkey imperatives vis-a-vis high standards in outsourcing. Dynamically exploit unique imperatives with dynamic systems. Appropriately formulate technically sound users and excellent expertise.</p>\n<p>Competently redefine long-term high-impact relationships rather than effective metrics. Distinctively maintain impactful platforms after strategic imperatives. Intrinsically evolve mission-critical deliverables after multimedia based e-business. Interactively mesh cooperative benefits whereas distributed process improvements. Progressively monetize an expanded array of e-services whereas.</p>', 'posts/March2018/weZwLLpaXnxyTR989iDk.jpg', 'creating-something-useful', 'Find out how to Create something useful', 'useful, create something useful', 'PUBLISHED', 0, '2018-03-25 20:49:37', '2018-03-25 20:56:38'),
(10, 1, 1, 'Never Stop Creating', 'Never Stop Creating', NULL, '<p>The reason why we are the way we are is... Because we are designed for a purpose. Some people are created to help or service, and others are created to... Well... Create. Are you a creator.</p>\n<p>If you have a passion for creating new things and bringing ideas to life. You\'ll want to save yourself some time by using Wave to build the foundation. Wave has so many built-in features including Billing, User Profiles, User Settings, an API, and so much more.</p>\n<blockquote>\n<p>You may have a need to only want to make money from your application, but if your application can help others achieve a goal and you can make money from it too, you have a gold-mine.</p>\n</blockquote>\n<p>Some more info on your awesome post here. After this sentence, it\'s just going to be a little bit of jibberish. But you get a general idea. You\'ll want to blog about stuff to get your customers interested in your application. With leverage existing reliable initiatives before leveraged ideas. Rapidiously develops equity invested expertise rather than enabled channels. Monotonectally intermediate distinctive networks before highly efficient core competencies.</p>\n<h2>Seamlessly promote flexible growth strategies.</h2>\n<p><img src=\"/storage/posts/March2018/blog-1.jpg\" alt=\"blog\" /></p><p>Dramatically harness extensive value through the fully researched human capital. Seamlessly transition premium schemas vis-a-vis efficient convergence. Intrinsically build competitive e-commerce with cross-unit information. Collaboratively e-enable real-time processes before extensive technology. Authoritatively fabricate efficient metrics through intuitive quality vectors.</p>\n<p>Collaboratively deliver optimal vortals whereas backward-compatible models. Globally syndicate diverse leadership rather than high-payoff experiences. Uniquely pontificate unique metrics for cross-media human capital. Completely procrastinate professional collaboration and idea-sharing rather than 24/365 paradigms. Phosfluorescently initiates multimedia based outsourcing for interoperable benefits.</p>\n<h3>Seamlessly promote flexible growth strategies.</h3>\n<p>Progressively leverage other\'s e-business functionalities through corporate e-markets. Holistic repurpose timely systems via seamless total linkage. Appropriately maximize impactful \"outside the box\" thinking vis-a-vis visionary value. Authoritatively deploy interdependent technology through process-centric \"outside the box\" thinking. Interactively negotiate pandemic internal or \"organic\" sources whereas competitive relationships.</p>\n<figure><img src=\"/storage/posts/March2018/blog-2.jpg\" alt=\"wide\" />\n<figcaption>Keep working until you find success.</figcaption>\n</figure>\n<p>Enthusiastically deliver viral potentialities through multidisciplinary products. Synergistically plagiarize client-focused partnerships for adaptive applications. Seamlessly morph process-centric synergy whereas bricks-and-clicks deliverables. Continually disintermediate holistic action items without distinctive customer service. Enthusiastically seize enterprise web-readiness without effective schemas.</p>\n<h4>Seamlessly promote flexible growth strategies.</h4>\n<p>Assertively restore installed base data before sustainable platforms. Globally recapitalize orthogonal systems via clicks-and-mortar web services. Efficiently grow visionary action items through collaborative e-commerce. Efficiently architect highly efficient \"outside the box\" thinking before customer directed infomediaries. Proactively mesh holistic human capital rather than exceptional niches.</p>\n<p>Intrinsically create innovative value and pandemic resources. Progressively productize turnkey e-markets and economically sound synergy. Objectively supply turnkey imperatives vis-a-vis high standards in outsourcing. Dynamically exploit unique imperatives with dynamic systems. Appropriately formulate technically sound users and excellent expertise.</p>\n<p>Competently redefine long-term high-impact relationships rather than effective metrics. Distinctively maintain impactful platforms after strategic imperatives. Intrinsically evolve mission-critical deliverables after multimedia based e-business. Interactively mesh cooperative benefits whereas distributed process improvements. Progressively monetize an expanded array of e-services whereas.</p>', 'posts/March2018/K804BvnOehlLao0XmI08.jpg', 'never-stop-creating', 'In this article you\'ll learn how important it is to never stop creating', 'creating, never stop', 'PUBLISHED', 0, '2018-03-25 20:08:02', '2018-06-28 00:14:31');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin User', '2017-11-21 10:23:22', '2017-11-21 10:23:22'),
(2, 'trial', 'Free Trial', '2017-11-21 10:23:22', '2017-11-21 10:23:22'),
(3, 'basic', 'Basic Plan', '2018-07-02 23:03:21', '2018-07-03 11:28:44'),
(4, 'pro', 'Pro Plan', '2018-07-03 10:27:16', '2018-07-03 11:28:38'),
(5, 'premium', 'Premium Plan', '2018-07-03 10:28:42', '2018-07-03 11:28:32'),
(6, 'cancelled', 'Cancelled User', '2018-07-03 10:28:42', '2018-07-03 11:28:32');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Wave', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'The Software as a Service Starter Kit built on Laravel & Voyager', '', 'text', 2, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(6, 'admin.title', 'Admin Title', 'Wave', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Create some waves and build your next great idea', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', '', '', 'text', 1, 'Admin'),
(11, 'site.favicon', 'Favicon', '', NULL, 'image', 6, 'Site'),
(12, 'auth.dashboard_redirect', 'Homepage Redirect to Dashboard if Logged in', '0', NULL, 'checkbox', 7, 'Auth'),
(13, 'auth.email_or_username', 'Users Login with Email or Username', 'email', '{\n\"default\" : \"email\",\n\"options\" : {\n\"email\": \"Email Address\",\n\"username\": \"Username\"\n}\n}', 'select_dropdown', 8, 'Auth'),
(14, 'auth.username_in_registration', 'Username when Registering', 'yes', '{\n\"default\" : \"yes\",\n\"options\" : {\n\"yes\": \"Yes, Include the Username Field when Registering\",\n\"no\": \"No, Have it automatically generated\"\n}\n}', 'select_dropdown', 9, 'Auth'),
(15, 'auth.verify_email', 'Verify Email during Sign Up', '0', NULL, 'checkbox', 10, 'Auth'),
(16, 'billing.card_upfront', 'Require Credit Card Up Front', '1', '{\n\"on\" : \"Yes\",\n\"off\" : \"No\",\n\"checked\" : false\n}', 'checkbox', 11, 'Billing'),
(17, 'billing.trial_days', 'Trial Days when No Credit Card Up Front', '-1', NULL, 'text', 12, 'Billing');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `version` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `folder`, `active`, `version`, `created_at`, `updated_at`) VALUES
(1, 'Tailwind Theme', 'tailwind', 1, '1.0', '2020-08-23 02:06:45', '2020-08-23 02:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `theme_options`
--

CREATE TABLE `theme_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme_id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `theme_options`
--

INSERT INTO `theme_options` (`id`, `theme_id`, `key`, `value`, `created_at`, `updated_at`) VALUES
(17, 1, 'logo', '', '2017-11-22 10:54:46', '2018-02-10 23:02:40'),
(18, 1, 'home_headline', 'Welcome to Wave', '2017-11-25 11:31:45', '2018-08-27 18:17:41'),
(19, 1, 'home_subheadline', 'Start crafting your next great idea.', '2017-11-25 11:31:45', '2017-11-26 01:11:47'),
(20, 1, 'home_description', 'Wave will help you rapidly build a Software as a Service. Out of the box Authentication, Subscriptions, Invoices, Announcements, User Profiles, API, and so much more!', '2017-11-25 11:31:45', '2017-11-26 01:09:50'),
(21, 1, 'home_cta', 'Signup', '2017-11-25 14:02:29', '2020-10-23 14:17:25'),
(22, 1, 'home_cta_url', '/register', '2017-11-25 14:09:33', '2017-11-26 10:12:41'),
(23, 1, 'home_promo_image', 'themes/February2018/mFajn4fwpGFXzI1UsNH6.png', '2017-11-25 15:36:46', '2017-11-28 19:17:00'),
(24, 1, 'footer_logo', 'themes/August2018/TksmVWMqp5JXUQj8C6Ct.png', '2018-08-28 17:12:11', '2018-08-28 17:12:11');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 1, 'pt', 'Post', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(2, 'data_types', 'display_name_singular', 2, 'pt', 'Página', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(3, 'data_types', 'display_name_singular', 3, 'pt', 'Utilizador', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(5, 'data_types', 'display_name_singular', 5, 'pt', 'Menu', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(6, 'data_types', 'display_name_singular', 6, 'pt', 'Função', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(7, 'data_types', 'display_name_plural', 1, 'pt', 'Posts', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(8, 'data_types', 'display_name_plural', 2, 'pt', 'Páginas', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(9, 'data_types', 'display_name_plural', 3, 'pt', 'Utilizadores', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(11, 'data_types', 'display_name_plural', 5, 'pt', 'Menus', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(12, 'data_types', 'display_name_plural', 6, 'pt', 'Funções', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(22, 'menu_items', 'title', 3, 'pt', 'Publicações', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(23, 'menu_items', 'title', 4, 'pt', 'Utilizadores', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(24, 'menu_items', 'title', 5, 'pt', 'Categorias', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(25, 'menu_items', 'title', 6, 'pt', 'Páginas', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(26, 'menu_items', 'title', 7, 'pt', 'Funções', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(27, 'menu_items', 'title', 8, 'pt', 'Ferramentas', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(28, 'menu_items', 'title', 9, 'pt', 'Menus', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(29, 'menu_items', 'title', 10, 'pt', 'Base de dados', '2017-11-21 10:23:23', '2017-11-21 10:23:23'),
(30, 'menu_items', 'title', 13, 'pt', 'Configurações', '2017-11-21 10:23:23', '2017-11-21 10:23:23');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` datetime DEFAULT NULL,
  `verification_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `username`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `verification_code`, `verified`) VALUES
(1, 1, 'Wave Admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$L8MjmjVVOCbyLHbp7pq/9.1ZEEa5AqE67ZXLd2M4.res05a3Rz/G2', '4oXDVo48Lm1pc4j7NkWI9cMO4hv5OIEJFMrqjSCKQsIwWMGRFYDvNpdioBfo', NULL, '2017-11-21 10:07:22', '2018-09-22 17:34:02', 'admin', NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wave_key_values`
--

CREATE TABLE `wave_key_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyvalue_id` int(10) UNSIGNED NOT NULL,
  `keyvalue_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wave_key_values`
--

INSERT INTO `wave_key_values` (`id`, `type`, `keyvalue_id`, `keyvalue_type`, `key`, `value`) VALUES
(10, 'text_area', 1, 'users', 'about', 'Hello I am the admin user. You can update this information in the edit profile section. Hope you enjoy using Wave.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement_user`
--
ALTER TABLE `announcement_user`
  ADD KEY `announcement_user_announcement_id_index` (`announcement_id`),
  ADD KEY `announcement_user_user_id_index` (`user_id`);

--
-- Indexes for table `api_keys`
--
ALTER TABLE `api_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `api_tokens_token_unique` (`key`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Indexes for table `paddle_subscriptions`
--
ALTER TABLE `paddle_subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `paddle_subscriptions_subscription_id_unique` (`subscription_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_groups`
--
ALTER TABLE `permission_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permission_groups_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `plans_slug_unique` (`slug`),
  ADD KEY `plans_role_id_foreign` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `voyager_themes_folder_unique` (`folder`);

--
-- Indexes for table `theme_options`
--
ALTER TABLE `theme_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `voyager_theme_options_theme_id_index` (`theme_id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `wave_key_values`
--
ALTER TABLE `wave_key_values`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `wave_key_values_keyvalue_id_keyvalue_type_key_unique` (`keyvalue_id`,`keyvalue_type`,`key`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `api_keys`
--
ALTER TABLE `api_keys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `paddle_subscriptions`
--
ALTER TABLE `paddle_subscriptions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `permission_groups`
--
ALTER TABLE `permission_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `theme_options`
--
ALTER TABLE `theme_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wave_key_values`
--
ALTER TABLE `wave_key_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `announcement_user`
--
ALTER TABLE `announcement_user`
  ADD CONSTRAINT `announcement_user_announcement_id_foreign` FOREIGN KEY (`announcement_id`) REFERENCES `announcements` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `announcement_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `plans`
--
ALTER TABLE `plans`
  ADD CONSTRAINT `plans_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `theme_options`
--
ALTER TABLE `theme_options`
  ADD CONSTRAINT `theme_options_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
